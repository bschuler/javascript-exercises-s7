/**
 * 
 * @param {string} haystack 
 * @param {string} needle 
 * @param {number} startIndex 
 */
let contains = function(haystack, needle, startIndex = 0){
    let workingString = haystack.substring(startIndex,haystack.length);
    let index = findElementIndex(workingString,needle);
    if (index === undefined){
        return -1;
    }
    return index;
}


/**
 * 
 * @param {string} str 
 * @param {string} sep 
 * @returns 
 */
 let findElementIndex = function(str, sep){
    let i,j;
    for(i=0; i<str.length; i++){
        for(j=0; j<sep.length; j++){
            if(str[i+j] != sep[j]){
                break
            } else if(j == sep.length - 1) {
                return i;            
            } else if(i + j == str.length - 1) {
                break
            }            
        }        
    }
}

const dirtyString = "qwerqfasabc;ljoiuabc;ij;k;jlab"
console.log(contains(dirtyString,"abc",10));
console.log(dirtyString[17]);
console.log(contains(dirtyString,";"));
console.log(dirtyString[11]);