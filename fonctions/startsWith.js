import { assert } from "chai";

/**
 * 
 * @param {string} str 
 * @param {string} start 
 * @returns 
 */
 let startsWith = function(str, start){
    let j;
    for(j=0; j<start.length; j++){
        if(str[j] != start[j]){
            break
        } else if(j == start.length - 1) {
            return true;            
        } else if(j == str.length - 1) {
            break
        }            
    }        
    return false
}

const dirtyString = "qwerqfasabc;ljoiuabc;ij;k;jlab"
assert.isFalse(startsWith(dirtyString,"abc"));
assert.isFalse(startsWith(dirtyString,"def"));
assert.isTrue(startsWith(dirtyString,"qwer"));