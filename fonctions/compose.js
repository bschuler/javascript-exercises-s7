import { expect } from 'chai';

/**
 * 
 * @param {function} f 
 * @param {function} g 
 * @returns function
 */
const compose = function(f, g) {
    return value => f(g(value));
};

const increment = x => x + 1;
const double = y => y * 2;
const timesTwoPlusOne = compose(increment, double);
expect(timesTwoPlusOne(5)).to.equal(11);