/**
 * 
 * @param {char} char 
 * @param {string} str 
 * @param {number} quantity 
 */
let padLeft = function(char, str, quantity) {
    let result = str;
    if(quantity < str.length){
        return console.error("expected length below real length");
    } else if (quantity > str.length) {
        while (quantity > result.length){
            result=char+result;
        }
    }
    return result;
}

let padZeros = (str, quantity) => padLeft("0", str, quantity);
let padSpace = (str, quantity) => padLeft("0", str, quantity);


const dirtyString = "qwerqfasabc;ljoiuabc;;;;ij;k;jlab"
console.log(dirtyString.length)
console.log(padLeft('F',dirtyString, 40));
console.log(padLeft('é',dirtyString, 35));
console.log(padLeft('4',dirtyString,10));