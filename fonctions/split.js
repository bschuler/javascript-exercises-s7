import { expect } from "chai";

/**
 * 
 * @param {string} str 
 * @param {string} sep 
 * @returns 
 */
const findElementIndex = function(str, sep){
    let i,j;
    for(i=0; i<str.length; i++){
        for(j=0; j<sep.length; j++){
            if(str[i+j] !== sep[j]){
                break
            } else if(j === sep.length - 1) {
                return i;            
            } else if(i + j === str.length - 1) {
                break
            }            
        }        
    }
}

/**
 * 
 * @param {string} str 
 * @param {string} sep 
 */
const split = function(str, sep){ 
    if(sep.length === 0){
        return [...str];
    }   
    const result = [];
    whileLoop: do {
        let index = findElementIndex(str, sep);
        if (index != null){
            result.push(str.substring(0,index));
            str = str.substring(index+sep.length, str.length);
        } else {
            result.push(str);
            break whileLoop;
        }
    } while (true);
    return result;   
}

const dirtyString = "qwerqfasabc;ljqqqoiu  abc;ij;k;jlab";
expect(split(dirtyString,"abc")).to.contain("qwerqfas", ";ljqqqoiu  ", ";ij;k;jlab");
expect(split(dirtyString,"abc").length).to.equal(3);
expect(split(dirtyString,";")).to.contain('qwerqfasabc', 'ljqqqoiu  abc', 'ij', 'k', 'jl');
expect(split(dirtyString,'')).to.contain('q','w','e','r','q','f','a','s','a','b','c',';','l','j','q','q','q','o','i','u',' ',' ','a','b','c',';','i','j',';','k',';','j','l','a','b');
expect(split(dirtyString,"q")).to.contain('', 'wer', 'fasabc;lj', '', '', 'oiu  abc;ij;k;jlab');
expect(split(dirtyString," ")).to.contain('qwerqfasabc;ljqqqoiu', '', 'abc;ij;k;jlab');