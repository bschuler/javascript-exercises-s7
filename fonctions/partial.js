import { expect } from "chai";

const partial = function(f, ...arg) {
    return (...args) => f(...arg, ...args);
};

const f = (x, y, z) => x * (y - z);
expect(partial(f, 2)(3, 4)).to.equal(-2);