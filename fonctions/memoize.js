import { expect } from "chai";

const memoize = (fn) => {
    let cache = {};
    return (...args) => {
      let n = args[0];
      if (n in cache) {
        console.log('Fetching from cache', n);
        return cache[n];
      }
      else {
        console.log('Calculating result', n);
        let result = fn(n);
        cache[n] = result;
        return result;
      }
    }
  }

const fibonacci = function(n){
    if (n < 2){
        return n
    } else {
        return fibonacci(n-1)+fibonacci(n-2);
    }
};

const fibonacci2 = memoize((n) => {
    if (n < 2){
        return n
    } else {
        return fibonacci2(n-1)+fibonacci2(n-2);
    }
});

  console.log(fibonacci(5)); // calculated
  console.log(fibonacci(6)); // calculated for 6 and cached for 5
  console.log(fibonacci2(5));
  console.log(fibonacci2(6));
  console.log(fibonacci2(7));
  console.log(fibonacci2(30));
  console.log(fibonacci2(40));