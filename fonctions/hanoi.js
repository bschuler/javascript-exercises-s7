import { expect } from "chai";

/**
 * 
 * @param {number} n 
 * @param {number} D 
 * @param {number} A 
 * @param {number} I 
 * @returns 
 */
const hanoiRecNumbers = function(n, D, A, I){
    if(n === 0){
        return
    }
    hanoiRecNumbers(n-1, D, I, A);
    console.log(D+" -> "+A);
    hanoiRecNumbers(n-1, I, A, D);
}

/**
 * 
 * @param {number} n 
 * @param {number} D 
 * @param {number} A 
 */
const hanoi = function(n, D, A) {
    if (!([1,2,3].includes(D) && [1,2,3].includes(A)) || D === A){
        console.log("A and D must be positive distinct integers between 1 and 3")
        return
    }
    if (n <= 0){
        console.log("n must be a positive integer")
        return
    }
    let I;
    switch(A+D){
        case(3): I=3; break;
        case(4): I=2; break;
        case(5): I=1; break;
    }
    console.log("hanoi starts with : n="+n+", D="+D+", A="+A+", I="+I);
    hanoiRecNumbers(n, D, A, I);
    console.log("hanoi done with : n="+n+", D="+D+", A="+A+", I="+I);
}

hanoi(3,1,2);
hanoi(2, 3, 2);
hanoi(2,3,1);
hanoi(2,3,2);
hanoi(2,1,3);
hanoi(2,2,3);
hanoi(2, 2, 1);
hanoi(5, 3, 3);
hanoi(0,1,3);