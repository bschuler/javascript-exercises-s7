/**
 * 
 * @param {char} char 
 * @param {string} str 
 * @param {number} quantity 
 */
 let padRight = function(char, str, quantity) {
    let result = str;
    if(quantity < str.length){
        return console.error("expected length below real length");
    } else if (quantity > str.length) {
        while (quantity > result.length){
            result+=char;
        }
    }
    return result;
}


let padZeros = (str, quantity) => padRight("0", str, quantity);
let padSpace = (str, quantity) => padRight("0", str, quantity);


const dirtyString = "qwerqfasabc;ljoiuabc;;;;ij;k;jlab"
console.log(dirtyString.length)
console.log(padRight('F',dirtyString, 40));
console.log(padRight('é',dirtyString, 35));
console.log(padRight('4',dirtyString,10));