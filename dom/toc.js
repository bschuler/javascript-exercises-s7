let patterns = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']
patternsToFind = () => {
    let result="";
    for(let pattern of patterns){
        result+=pattern+" ,"; //push stringbuilder
    }
    result = result.slice(0, result.length-2);
    return result;
}

let elements = document.querySelectorAll(patternsToFind());
let toc = document.createElement('ol');
let structure = []
for(element of elements){
    if(element.innerText !== undefined){
        let listItem = document.createElement('li');
        listItem.innerText = element.innerText.toUpperCase()+" is a "+element.tagName+", "+element.tagName[1]+", "+typeof element.tagName[1]+", "+parseInt(element.tagName[1], 10);
        structure.push[parseInt(element.tagName[1], 10)];
        toc.append(listItem);
    }
}
let title = document.querySelector('h1');
title.appendChild(toc);
console.log(structure);