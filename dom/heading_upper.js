const patterns = ['title', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']
patternsToFind = () => {
    let result="";
    for(let pattern of patterns){
        result+=pattern+" ,";
    }
    result = result.slice(0, result.length-2);
    return result;
}

const elements = document.querySelectorAll(patternsToFind());
for(element of elements){
    if(element.innerText !== undefined){
        element.innerText = element.innerText.toUpperCase();
    }
}