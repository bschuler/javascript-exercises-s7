const convertRec = function(myNode){
    if(myNode.nodeType === Node.TEXT_NODE){
        myNode.nodeValue = myNode.nodeValue.toUpperCase();
    }
    let children = myNode.childNodes;
    if(children.length >= 0){
        for(child of children){ convertRec(child)}
    }
}

convertRec(document)