import { assert } from 'chai';

class EZArray extends Array {

    first(){
        return this[0];
    }
    last(){
        return this[this.length-1];
    }
}

let a = new EZArray();
assert.isTrue(a instanceof EZArray);  // => true: instance de EZArray
assert.isTrue(a instanceof Array);    // => true: instance de Array aussi
a.push(1,2,3,4);      // a.length == 4; méthodes hérités
assert.equal(a.pop(),4);               // => 4
assert.equal(a.first(),1);               // => 1: first getter
assert.equal(a.last(),3);                // => 3: last getter
assert.equal(a[1],2);                  // => 2: syntaxe habituelle d'accès aux élément du tableau
assert.isTrue(Array.isArray(a));      // => true
assert.isTrue(EZArray.isArray(a));    // => true