import { assert } from 'chai';

class Range {

    #from
    #to

    // Initialise l'objet
    constructor(from, to) {
		this.from = from;
		this.to = to;
	}

    // Dit si `x` tombe dans l'interval [from; to]
    includes(x) {
		return this.from <= x && x <= this.to;
	}

    // Produit une chaîne de caractère sous la forme "(from...to)", par exemple "(2...8)"
    toString() {
        return "("+this.from+"..."+this.to+")";
    }

    // Analyse une chaîne de caractère au format "(from...to)" et retourne une nouvelle instance
    static parse(s){
        const parsedArray = Range.integerRangePattern.exec(s);
        if (Array.isArray(parsedArray)){
            return new Range(parsedArray[1],parsedArray[2]);
        }
    }

    // RegExp pour analyser la chaîne avec parse
    static integerRangePattern = /^\((-?\d+)...(-?\d+)\)$/;
}

class Span extends Range {
    constructor(departure, length){
        if(length >= 0){
            super(departure,departure+length);
        } else {
            super(departure+length,departure);
        }
    }
}

let myRange = new Range(1,3);
const mastring = "(132...123451)";
const newRange = Range.parse(mastring);
assert.isTrue(newRange instanceof(Range));
assert.deepEqual(myRange.toString(),"(1...3)");
assert.deepEqual(newRange.toString(),mastring);

assert.deepEqual((new Span(2, 4)).toString(),"(2...6)");
assert.deepEqual((new Span(12, -8)).toString(),"(4...12)");