import { assert } from 'chai';
import _ from 'lodash';

Object.prototype.forEachOwnProperty = function(f) {
    for(let i of Object.keys(this)){
        f(i)
    }
}

const o1 = { a: 1 };
const o2 = Object.create(o1);
o2.b = 2;
o2.c = 3;

const props = [];
o2.forEachOwnProperty(prop => props.push(prop));
assert.equal(_.isEqual(props, ['b','c']),true); // => ["b", "c"]; mais pas "a"  