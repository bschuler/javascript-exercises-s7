import { assert } from 'chai';
import _ from 'lodash';

/**
 * 
 * @param {number} size 
 * @param {function name(params) {
     
 }} f 
 */
const initWith = function(size, f){
    const result = [];
    for(let i of _.range(size)){
        result.push(f(i))
    }
    return result;
}

const withZero = () => 0;
const fromZero = index => index;
const from42 = index => 42 + index;
assert.equal(_.isEqual(initWith(5, withZero),[0, 0, 0, 0, 0]), true);
assert.equal(_.isEqual(initWith(5, fromZero),[0, 1, 2, 3, 4]),true);
assert.equal(_.isEqual(initWith(5, from42),[42, 43, 44, 45, 46]),true);
