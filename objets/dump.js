import _ from 'lodash';
import { expect, assert } from 'chai';


/**
 * 
 * @param {Object} obj 
 * @returns 
 */
const dump = function(obj){
    let result = "{";
    for (let key in obj){
        result += key+":"
        if (typeof obj[key] == "string"){
            result += "\""+obj[key]+"\""
        } else if (Array.isArray(obj[key])){
            result += "["+obj[key]+"]"
        } else {
            result +=obj[key]
        }
        result +=","
    }
    result = result.slice(0,-1);
    result +="}"
    return result;
}

let obj = {};
obj.firstname = "Alan";
obj.lastname = "Turing";
obj.birthday = [1921, 6, 23];
console.log(dump(obj)); // => {firstname:"Alan",lastname:"Turing",birthday:[1921,6,23]}
assert.equal(_.isEqual(dump(obj),"{firstname:\"Alan\",lastname:\"Turing\",birthday:[1921,6,23]}"),true);
