import { assert } from 'chai';
import _ from 'lodash';

const initWith = function(f){
    return function(size, departure = 0){
        const result = [];
        for(let i of _.range(size)){
            result.push(f(i+departure))
        }
        return result;
    }
}

const initWithZeros = initWith(() => 0);
const initFrom = initWith(value => value);

assert.equal(_.isEqual(initWithZeros(3),[0, 0, 0]),true);
assert.equal(_.isEqual(initFrom(3, 42),[42, 43, 44]),true);
