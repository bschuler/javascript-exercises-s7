import { expect } from 'chai';
import _ from 'lodash';

/**
 * Expect a and b to be only Arrays, containing basic values and/or other likewise Arrays, no Objects
 * @param {Array} a 
 * @param {Array} b 
 * @returns 
 */
function deepEqual(a, b) {
    if (a === b) return true;
    if (a.length !== b.length) return false;

    for (let i of _.range(a.length) ) {
        if (Array.isArray(a[i]) && Array.isArray(b[i])){
            if (deepEqual(a[i],b[i]) !== true) {
                return false
            }
        } else {
            if (a[i] !== b[i]) return false;
        }
    }
    return true;
}

const a1 = [1, 2, [3, 4], 5, "hello", -234.1234, []];
const a2 = JSON.parse(JSON.stringify(a1)); // Deep copy
expect(a1 === a2).to.be.false; // => false
expect(deepEqual(a1, a2)).to.be.true; // => true

const a3 = [1,2,3,4,5,6,7];
expect(deepEqual(a1,a3)).to.be.false;