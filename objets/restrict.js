import { assert } from 'chai';
import _ from 'lodash';

const restrict = function(target, keep){
    const propertiesToKeep = Object.keys(keep);
    const propertiesToRestrict = Object.keys(target);
    for(let indexRestrict of _.range(propertiesToRestrict.length)){
        if (!propertiesToKeep.includes(propertiesToRestrict[indexRestrict])){
            const propertyName = propertiesToRestrict[indexRestrict];
            delete target[propertyName];
        }
    }
    return target;
}

const config = { user: "user", pass: "pass" };
const tooMuchConfig = { vars: "LOG=info", user: "user", pass: "pass", env: "prod" };
assert.isTrue("vars" in tooMuchConfig); // => true
assert.isTrue("env" in tooMuchConfig); // => true

const properConfig = restrict(tooMuchConfig, config);
assert.isFalse(properConfig === config); // => false
assert.isFalse("vars" in properConfig); // => false
assert.isFalse("env" in properConfig); // => false