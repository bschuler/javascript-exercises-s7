/**
 * 
 * @param {Object} source 
 * @param {*} propertyNames 
 * @returns 
 */
const diff = function(source, propertyNames){
    for (const property in propertyNames){
        delete source[property];
    }
    return source;
}

let o1 = { r: 0, g: 0, b: 0, a: 0 };
let withoutOpacity = { a: null };
let objectWithoutOpacity = diff(o1, withoutOpacity); // => { r: 0, g: 0, b: 0 }
console.log(o1);
console.log(objectWithoutOpacity);
o1 === objectWithoutOpacity;

let o2 = { r: 0, g: 0, b: 0, a: 0 };
let o3 = { r: 0, b: 0, a: 0 };
let newObject = diff(o2,o3);
console.log(newObject);