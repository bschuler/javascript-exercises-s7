import { assert } from 'chai';

/**
 * @param {Iterable} iterable Chaîne de caractère, Array, Map, Set
 * @param {function} f Fonction qui transforme chaque élément.
 */
function filter(iterable, f) {
    let index = -1;
	return {
		next() {
            do {
                index ++;
                if (!f(iterable[index])){
                    continue
                }
                return { done: false, value: iterable[index] }
            } while (index != iterable.length);
            return { done: true }
		}
	}
}

const iterator = filter("hello", v => "aeiouy".includes(v));
assert.equal(iterator.next().value, 'e'); // => "e"
assert.equal(iterator.next().value, 'o'); // => "o"
assert.isTrue(iterator.next().done); // => true