import { assert } from 'chai';
import _ from 'lodash';

/**
 * @param {Iterable} iterable Chaîne de caractère, Array, Map, Set
 * @param {function} f Fonction qui transforme chaque élément.
 */
 function map(iterable, f) {
    const iterator = iterable[Symbol.iterator]();
    let nexter = iterator.next();
    let transformedIterable = [];

    while (!nexter.done) {
        transformedIterable.push(f(nexter.value));
        nexter = iterator.next();
    }
    return transformedIterable;
}

const numbers = _.range(25);
const square = (myNumber) => myNumber**2;
const squares = map(numbers,square);
assert.isTrue(squares instanceof Array);
assert.deepEqual(squares,[
    0,   1,   4,   9,  16,  25,  36,
   49,  64,  81, 100, 121, 144, 169,
  196, 225, 256, 289, 324, 361, 400,
  441, 484, 529, 576
]);

// Create a iterable object (similar to the class Range).
function getSequenceIterable(from, to) {
    return {
        [Symbol.iterator]: () => ({
            next() {
                return {
                    done: from > to,
                    value: from++,
                };
            }
        })
    };
}

const helloIterable = map("hello", v => v.toUpperCase());
const iterator = helloIterable[Symbol.iterator]();
assert.strictEqual(iterator.next().value, "H");
assert.strictEqual(iterator.next().value, "E");
assert.strictEqual(iterator.next().value, "L");
assert.strictEqual(iterator.next().value, "L");
assert.strictEqual(iterator.next().value, "O");
assert.strictEqual(iterator.next().done, true);

const myIterable = getSequenceIterable(4, 8);
assert.deepEqual([...map(myIterable, x => x * 2)], [8, 10, 12, 14, 16]);